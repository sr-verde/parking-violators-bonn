import csv
import urllib.request
import json
import re
import time

import concurrent.futures
from typing import List, Dict, Any
from datetime import datetime


def cleanup_address_string(addr_row: str) -> str:
    address = re.sub("Bonn\s*,\s+", "", addr_row, flags=re.IGNORECASE)
    address = re.sub("Hinter \??Laterne [0-9]+", "", address, flags=re.IGNORECASE)
    address = re.sub("Vor \??Laterne [0-9]+", "", address, flags=re.IGNORECASE)
    address = re.sub("\?", "", address)
    address = re.sub("Laterne [0-9]+", "", address, flags=re.IGNORECASE)
    address = re.sub("ggü\.? Hnr.", "", address, flags=re.IGNORECASE)
    address = re.sub("ggü\.? Nr.", "", address, flags=re.IGNORECASE)
    address = re.sub("ggü\.?", "", address, flags=re.IGNORECASE)
    address = re.sub("gegenüber Hnr\.?", "", address, flags=re.IGNORECASE)
    address = re.sub("gegenüber Nr\.?", "", address, flags=re.IGNORECASE)
    address = re.sub("gegenüber", "", address, flags=re.IGNORECASE)
    address = re.sub("gegen", "", address, flags=re.IGNORECASE)
    address = re.sub("/[0-9]+", "", address, flags=re.IGNORECASE)

    address += ", Bonn, Deutschland"

    return address


def store_to_json(marker_data: List, file_path: str) -> float:
    with open(file_path, "w") as marker_file:
        json.dump(marker_data, marker_file)

    return time.time()


def parse_line(row) -> Dict[str, Any]:
    address = cleanup_address_string(row[2])

    headers = {"User-Agent": "Bonner Falschparker"}
    url = f"https://nominatim.bonnbike.com/search/{urllib.parse.quote(address)}?format=json"
    req = urllib.request.Request(url, headers=headers)

    with urllib.request.urlopen(req) as data:
        data = json.loads(data.read().decode())

        if data == []:
            coords = None
        else:
            coords = {"lat": data[0]["lat"], "lon": data[0]["lon"]}

        t = row[1]

        if len(t) < 4:
            t = "0" + t

        dt = datetime.strptime(row[0] + " " + t, "%d.%m.%Y %H%M")
        print(dt)
        dt = dt.timestamp()

        try:
            fine = int(row[4])
        except ValueError:
            fine = float(row[4].replace(",", "."))

        marker = {
            "datetime": dt,
            "address": address,
            "coords": coords,
            "tbnr": int(row[3]),
            "fine": fine,
            "type": row[5],
        }

    return marker


last_storing = time.time()
marker_data = []
marker_file_path = "/home/verde/html/bonnbike/fp-map/markers_2019.json"


with open(marker_file_path, "r") as json_file:
    marker_data = json.load(json_file)


with open("Parkverstoesse2019.csv", newline="\n", encoding="utf-8") as csv_file:
    reader = csv.reader(csv_file, delimiter=";")
    next(reader)  # skip the first line that is the header

    if len(marker_data) > 0:
        for i in range(len(marker_data)):
            next(reader)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for row in reader:
            futures.append(executor.submit(parse_line, row=row))

        for future in concurrent.futures.as_completed(futures):
            marker_data.append(future.result())

store_to_json(marker_data, marker_file_path)
