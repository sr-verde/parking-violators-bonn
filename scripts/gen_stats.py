#! /usr/bin/env python

import json
import argparse
from csv import DictReader
from typing import Any, Dict, List, Union
from datetime import datetime

from pandas import DataFrame  # type: ignore
from matplotlib import pyplot as plt


WEEKDAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


def read_data(filepaths: List[str]) -> List[Dict[str, Any]]:
    data_list = []
    for fp in filepaths:
        with open(fp, newline='') as csv_file:
            reader = DictReader(csv_file, delimiter=';')
            data_list += clean_data(reader)
    return data_list


def clean_data(reader: DictReader) -> List[Dict[str, Any]]:
    """
    Cleans available data:
        - Make all keys lowercase
        - Cast from string to right data type
    """
    data_list = []
    for dp in list(reader):
        fine: Dict[str, Any] = {key.lower(): value for key, value in dp.items()}
        fine['geldbusse'] = float(fine['geldbusse'].replace(',', '.'))
        fine['tatbestandbe_tbnr'] = int(fine['tatbestandbe_tbnr'])
        data_list.append(fine)
    return data_list


def add_date_data(data: List[Dict[str, Any]]):
    for dp in data:
        dt_str = f"{dp['tattag']} {dp['tatzeit']:>04}"
        dt = datetime.strptime(dt_str, '%d.%m.%Y %H%M')
        dp['date'] = dt
        dp['month'] = dt.strftime('%m')
        dp['weekday'] = dt.strftime('%a')
        dp['hour'] = dt.strftime('%H')


def total_fines(data: DataFrame):
    return data['geldbusse'].sum()


def by_months(data: DataFrame) -> Dict[int, int]:
    return {month: len(data[data['month'] == f'{month:02}']) for month in range(1, 13)}


def by_weekdays(data: DataFrame) -> Dict[str, int]:
    return {wd: len(data[data['weekday'] == wd]) for wd in WEEKDAYS}


def by_hours(data: DataFrame) -> Dict[str, int]:
    return {f'{hour:02}': len(data[data['hour'] == f'{hour:02}']) for hour in range(24)}


def by_charge(data: DataFrame) -> Dict[int, int]:
    return {tbnr: len(data[data['tatbestandbe_tbnr'] == tbnr]) for tbnr in set(data['tatbestandbe_tbnr'])}


def draw_barchart(
    xvals: List[int],
    yvals: List[Union[int, float]],
    xtlabels: List[str],
    target_name: str,
    figure_labels: Dict[str, str],
    rotate_xtlabels: bool = False,
) -> None:
    plt.figure(figsize=(10, 7), dpi=800)
    plt.bar(
        xvals,
        height=yvals,
    )
    plt.xticks(
        ticks=xvals, labels=xtlabels, rotation=30 if rotate_xtlabels else 0, ha='right' if rotate_xtlabels else 'center'
    )
    plt.grid(which='major', axis='y', linestyle='--')
    plt.title(figure_labels['title'])
    plt.xlabel(figure_labels['xlabel'])
    plt.ylabel(figure_labels['ylabel'])
    plt.tight_layout()
    plt.savefig(target_name, dpi=800, orientation='landscape')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', nargs='+', help='input file(s)')
    parser.add_argument('-c', '--charts', action='store_true', help='draw barcharts')
    parser.add_argument(
        '-s',
        '--silent',
        action='store_true',
        help='don\'t print anything to stdout. You might want to use \'--charts\' or \'--json\' with this',
    )
    parser.add_argument('--json', action='store', help='export statistics as JSON to FILENAME', metavar='FILENAME')
    args = parser.parse_args()
    if args.silent and not (args.charts or args.json):
        exit(0)

    data = read_data(args.input)
    add_date_data(data)
    df = DataFrame(data, columns=data[0].keys())

    num_fines = len(df)
    if not args.silent:
        print('Total:\n-------------------------')
        print(f'Number of fines: {num_fines:,}')
        print(f'Total fines: {total_fines(df):,.2f} €')

    charge_data = by_charge(df)
    if not args.silent:
        print('\nCharges:\n-------------------------')
        for tbnr, entries in sorted(charge_data.items(), key=lambda x: x[1], reverse=True):
            print(f'{tbnr} = {entries:7} ({entries / num_fines * 100:05.2f}%)')

    monthly_data = by_months(df)
    if not args.silent:
        print('\nMonths:\n-------------------------')
        for month, entries in monthly_data.items():
            print(f'{MONTHS[month - 1]} = {entries:7} ({entries / num_fines * 100:05.2f}%)')

    wd_data = by_weekdays(df)
    if not args.silent:
        print('\nWeekdays:\n-------------------------')
        for wd, entries in wd_data.items():
            print(f'{wd} = {entries:7} ({entries / num_fines * 100:05.2f}%)')

    hourly_data = by_hours(df)
    if not args.silent:
        print('\nTime of day:\n-------------------------')
        for hour, fines in hourly_data.items():
            print(f'{hour}:00–{hour}:59 = {fines:6} ({fines / num_fines * 100:05.2f}%)')

    if args.charts:
        draw_barchart(
            list(range(24)),
            list(hourly_data.values()),
            [f'{hour:02}:00–{hour:02}:59' for hour in range(24)],
            'hourly_fines.svg',
            {'title': '#Fines per hour', 'xlabel': 'Time of day', 'ylabel': '#Fines'},
            True,
        )

        draw_barchart(
            list(range(12)),
            list(monthly_data.values()),
            MONTHS,
            'monthly_fines.svg',
            {'title': '#Fines per month', 'xlabel': 'Month', 'ylabel': '#Fines'},
            False,
        )

        draw_barchart(
            list(range(7)),
            list(wd_data.values()),
            WEEKDAYS,
            'fines_per_weekday.svg',
            {'title': '#Fines per weekday', 'xlabel': 'Weekday', 'ylabel': '#Fines'},
            False,
        )

    if filename := args.json:
        stats_dict = {
            'number_fines': num_fines,
            'total_fines': total_fines(df),
            'monthly': monthly_data,
            'weekday': wd_data,
            'hourly': hourly_data,
            'charges': charge_data,
        }

        with open(filename, 'w') as json_file:
            json.dump(stats_dict, json_file)


if __name__ == '__main__':
    main()
