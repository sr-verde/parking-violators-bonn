import json


with open("markers_2019.json", "r") as j_file:
    data = json.load(j_file)


sticker_list = {}
for sinner in data:
    if not sinner["coords"]:
        continue
    lat = sinner["coords"]["lat"]
    lon = sinner["coords"]["lon"]
    if lat not in sticker_list:
        sticker_list[lat] = {}
    if lon not in sticker_list[lat]:
        sticker_list[lat][lon] = {"address": sinner["address"], "stickers": []}
    s = {
        "datetime": sinner["datetime"],
        "tbnr": sinner["tbnr"],
        "fine": sinner["fine"],
        "type": sinner["type"],
    }
    sticker_list[lat][lon]["stickers"].append(s)

with open("fp_2019_cmp.json", "w") as j_file:
    json.dump(sticker_list, j_file)
