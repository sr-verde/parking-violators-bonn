# Data

Here, I want to credit the sources of the used data.


## Parking Violations

Data are from [opendata.bonn.de](https://opendata.bonn.de).
All data are licensed as [Creative Commons CC Zero License (cc-zero)](https://opendefinition.org/licenses/cc-zero/).


## Charges

`bussgeldkatalog.csv` is created by Pascua Theus on basis of official data.
`charges.json` is taken from [weg.li](https://weg.li/api).
